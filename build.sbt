lazy val commonScalacOptions = Seq(
  "-feature",
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  // "-Ywarn-value-discard",
  "-Ywarn-unused",
  "-encoding", "utf8"
)

lazy val commonSettings = Seq(
  organization := "ai.lum",
  scalaVersion in ThisBuild := "2.11.11",
  version in ThisBuild := "0.0.1",
  // we want to use -Ywarn-unused-import most of the time
  scalacOptions ++= commonScalacOptions,
  scalacOptions += "-Ywarn-unused-import",
  // -Ywarn-unused-import is annoying in the console
  scalacOptions in (Compile, console) := commonScalacOptions,
  // show test duration
  testOptions in Test += Tests.Argument("-oD")
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(
    name := "assignment-3",
    aggregate in test := false
  )

libraryDependencies ++= {
  val procVer = "6.1.4"

  Seq(
    "org.clulab" %% "processors-main" % procVer,
    "org.clulab" %% "processors-corenlp" % procVer,
    "org.clulab" %% "processors-odin" % procVer,
    "org.clulab" %% "processors-modelsmain" % procVer,
    "org.clulab" %% "processors-modelscorenlp" % procVer,
    // testing
    "org.scalatest" %% "scalatest" % "2.2.6" % Test,
    // logging
    "ch.qos.logback" %  "logback-classic"  % "1.1.7",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0",
    // charts
    "com.github.wookietreiber" %% "scala-chart" % "latest.integration"
  )
}
