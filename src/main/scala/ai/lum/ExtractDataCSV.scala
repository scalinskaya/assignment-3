package ai.lum


import scala.io.Source
import java.io._

object ExtractDataCSV {

	def main(args: Array[String]): Unit = {
	
		println("Nume, Carte")
		val bufferedSource = io.Source.fromFile("testing.csv")
		
		for (line <- bufferedSource.getLines) {
			val cols = line.split(",").map(_.trim)
			println(s"${cols(0)}|${cols(1)}")
		}
		
    bufferedSource.close
	}
}
