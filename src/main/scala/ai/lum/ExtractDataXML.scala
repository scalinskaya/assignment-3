package ai.lum

import scala.io.Source
import java.io._
import java.io.File

object ExtractDataXML {


	def getListOfFiles(dir: String, extensions: List[String]): List[File] = {
		val currentDirectory = new java.io.File("." + dir).getCanonicalPath
		val d = new File(currentDirectory)
    	if(d.exists && d.isDirectory) 
    	d.listFiles.filter(_.isFile).toList.filter { file =>
        	extensions.exists(file.getName.endsWith(_))
    	}
    	else
    	{
    		println("No directory found")
    		List();
    	}
	}


	def extractAuthors(fileXML:scala.xml.Elem) : Array[String] = {
		var rez = Array[String]()
		val authors = fileXML \\ "contrib"
	  	for (aux <- authors)
	  		if (aux.attribute("contrib-type").mkString("") == "author")
	  			rez :+= (((aux \\ "surname").text).mkString("") + (" ") + ((aux \\ "given-names").text).mkString(""))
		return rez
	}

	def writeStuff(stuff:scala.xml.NodeSeq, title: String, pw: PrintWriter) = {
		pw.write(title + "\n")
		for (x <- stuff)
			pw.write(x.text + "\n")
		pw.write("\n")
	}

	 def main(args: Array[String]): Unit = {
	  	//val filelines = Source.fromFile("PMC3012991.nxml").getLines.toArray
	  	val extensions = List("nxml","xml");
	  	val fileList = getListOfFiles("/files", extensions);
	  	val resultsPath = new java.io.File("./results").getCanonicalPath
	  	for (fileName <- fileList) {
	  		val pw = new PrintWriter(new File((resultsPath.mkString("")+ "/" + fileName.getName).replaceAll(".xml", ".txt")))
	  		val filelines = Source.fromFile(fileName.getCanonicalPath).getLines.toList
	  		val fileXML = scala.xml.XML.loadString(filelines.mkString(""))
	  		val authors = extractAuthors(fileXML)
	  		pw.write("Authors: \n")
	  		for (x <- authors)
	  			pw.write(x + "\n")
	  		pw.write("\n")
	  		val abstr = fileXML \\ "abstract" \ "p"
	  		// println(abstr.text)
	  		writeStuff(abstr, "Abstract:",pw)
	  		// println("")

	  		val content = (fileXML \\ "body") \\ "p"
	  		// println(content.text)
	  		writeStuff(content, "Content:",pw)
	  		// println("")

	   		val titles = fileXML \\ "article-title"
	  		// println(titles.text)
	  		writeStuff(titles, "Title:",pw)
	  		pw.close()
	  	}
	}
	//pw.close()
	
}