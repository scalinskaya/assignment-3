## How to run XMLparser

```
sbt "runMain ai.lum.ExtractDataXML"
```
It will extract all the data from the subfolder "files" and create the ".txt" corespondent files in the "results" subfolder of the project.
## How to run CSVparser

```
sbt "runMain ai.lum.ExtractDataCSV "
```
